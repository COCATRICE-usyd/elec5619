## Game On ##

 
 
 * ### **Introduction**
 
     As we all know, the customer to customer business model is quite popular these days. Some successful example can be Airbnb, Gumtree & EBay. Basically, Users can interact & communicate with other individual users via the C2C platform. Rather than dealing with a business group, users can talk  directly to another end-user, which is more approachable \& negotiable. 

     Following this trending, our team finds that in the context of gaming PC market, millions of gamers have trouble about dealing with their out-of-date gaming parts, renewing their hardware or having a try on the newest released graphics card. Honestly, gaming set-ups are always expensive, even a DIY system is not affordable for most of the plain people. More seriously, even though plenty of people want to buy some professional gaming parts, they does not have a chance to try these parts in a real system. In other words, without a real experience, customers will never know how powerful is the desired gaming part supposed to be, neither will they buy it. 

     To figure out this issue & explore this potential gaming markets, our team is trying to build a platform that enable gamers to interact efficiently. We want them to share parts \& make friends via this platform. Some third party gaming PC hardware company may also be involved as sponsors. It is similar to the Airbnb in property renting market, but instead of building a Airbnb clone in gaming context, we will build a unique platform with fresh new revenue model \& design based on the social network architecture.  
     
 * ### **Feature Distribution**
    * Sathwik Singari
        * Search the nearby Listing
        * Certain Listing display
    * Tianzuo Wang
        * Landing Page
        * Login/Registration
        * Messenger
    * Joshua Hewitt
        * Listings
        * Profile (Update)
 * ### **Spring Configuration**
     * Mave Project
     * Hibernate Configuration
     * Spring Security Configuration
     * Thymeleaf Configuration
     * Code Structure
        * ![Screen Shot 2017-10-18 at 19.18.03.png](https://bitbucket.org/repo/BgbXBa5/images/2431378170-Screen%20Shot%202017-10-18%20at%2019.18.03.png)
     
     
 * ### **Issue Tracker**
     * We use the issues tracker integrating in Bitbucket to assign all the tasks, debug and configuration.
         [Issue Tracker](https://bitbucket.org/COCATRICE-usyd/elec5619/issues)
 * ### **Communication**
    * Informal
        * What's App 
    * Formal
        * Bitbucket Issue Tracker
        * Email
        * Google Drive for sharing learning resources
 * ### **Integration**
     Based on the feature distribution mentioned above, after our development, we started to combine our works together. We have been working seperate branches. Since we configure the framework perfectly from the start. All the files got allocated appropriately. The only thing we need to to at this stage is merge our branches and fix the conflicts. In the following section we will also do some integration testing to make sure all these stuff works.
 * ### **Installation**
    * Start MySQL server
    * Create a database called "gameon"
    * Change the MySQL user login credentails in database.properties under src/main/resources directory
    * Configure a tomcat server with war artifact
    * Start the server and the app is ready
 * ### **Unit Testing & Acceptance Testing**
    * Unit Testing
        * Model testing
        * DAO testing
        * Service testing
        * Controller testing
    * Integration Testing
        * Register
        * Login
        * Post an add
        * Messenger
            * New
            * Inbox
            * Outbox
            * Star
            * Trash
        * Update Address
        * Fina nearby gamers
        * etc