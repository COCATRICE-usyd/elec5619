import app.model.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ModelTest {

    @Test
    public void UserTest() {
       User user = new User();
       user.setFirstName("Tianzuo");
       user.setLastName("Wang");
       user.setUsername("test");
       user.setPassword("test");
       assertEquals(user.getFirstName(), "Tianzuo");
       assertEquals(user.getLastName(), "Wang");
       assertEquals(user.getUsername(), "test");
       assertEquals(user.getPassword(), "test");
    }


}
