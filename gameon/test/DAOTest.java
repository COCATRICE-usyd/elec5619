import app.dao.UserDAO;
import app.dao.UserDAOImpl;
import app.model.Role;
import app.model.User;

import app.service.RoleService;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
@ContextConfiguration(locations =
        {"file:src/main/webapp/WEB-INF/gameon-mvc-test.xml"})

public class DAOTest {

    @Autowired
    UserDAO userDAO;

    @Autowired
    RoleService roleService;

    @Test
    @Transactional
    public void UserDaoTest() {
        User user = new User();
        user.setFirstName("David");
        user.setLastName("Lee");
        user.setUsername("cocatrice");
        user.setPassword("123456");
        user.setRole(roleService.getRole(2));
        userDAO.addUser(user);
        int id = user.getId();
        User inserted_user = userDAO.getUserById(id);
        assertEquals(inserted_user.getUsername(), user.getUsername());
    }
}
