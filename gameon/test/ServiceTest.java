import app.dao.UserDAO;
import app.dao.UserDAOImpl;
import app.model.Role;
import app.model.User;

import app.service.RoleService;
import app.service.UserService;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
@ContextConfiguration(locations =
        {"file:src/main/webapp/WEB-INF/gameon-mvc-test.xml"})

public class ServiceTest {

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    public User user;

    @Before
    public void setup(){

        this.user = new User();
        this.user.setFirstName("David");
        this.user.setLastName("Lee");
        this.user.setUsername("cocatrice");
        this.user.setPassword("123456");
        this.user.setRole(roleService.getRole(2));
        userService.addUser(this.user);
    }

    @Test
    @Transactional
    public void UserDaoTest() {
        int id = this.user.getId();
        User inserted_user = userService.getUserById(id);
        assertEquals(inserted_user.getUsername(), this.user.getUsername());
    }
}
