result_markers = [];
currPos = "";

function initMap() {
    var mapStyles = [
        {elementType: 'geometry', stylers: [{color: '#ebe3cd'}]},
        {elementType: 'labels.text.fill', stylers: [{color: '#523735'}]},
        {elementType: 'labels.text.stroke', stylers: [{color: '#f5f1e6'}]},
        {
            featureType: 'administrative',
            elementType: 'geometry.stroke',
            stylers: [{color: '#c9b2a6'}]
        },
        {
            featureType: 'administrative.land_parcel',
            elementType: 'geometry.stroke',
            stylers: [{color: '#dcd2be'}]
        },
        {
            featureType: 'administrative.land_parcel',
            elementType: 'labels.text.fill',
            stylers: [{color: '#ae9e90'}]
        },
        {
            featureType: 'landscape.natural',
            elementType: 'geometry',
            stylers: [{color: '#dfd2ae'}]
        },
        {
            featureType: 'poi',
            elementType: 'geometry',
            stylers: [{color: '#dfd2ae'}]
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{color: '#93817c'}]
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry.fill',
            stylers: [{color: '#a5b076'}]
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{color: '#447530'}]
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{color: '#f5f1e6'}]
        },
        {
            featureType: 'road.arterial',
            elementType: 'geometry',
            stylers: [{color: '#fdfcf8'}]
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{color: '#f8c967'}]
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{color: '#e9bc62'}]
        },
        {
            featureType: 'road.highway.controlled_access',
            elementType: 'geometry',
            stylers: [{color: '#e98d58'}]
        },
        {
            featureType: 'road.highway.controlled_access',
            elementType: 'geometry.stroke',
            stylers: [{color: '#db8555'}]
        },
        {
            featureType: 'road.local',
            elementType: 'labels.text.fill',
            stylers: [{color: '#806b63'}]
        },
        {
            featureType: 'transit.line',
            elementType: 'geometry',
            stylers: [{color: '#dfd2ae'}]
        },
        {
            featureType: 'transit.line',
            elementType: 'labels.text.fill',
            stylers: [{color: '#8f7d77'}]
        },
        {
            featureType: 'transit.line',
            elementType: 'labels.text.stroke',
            stylers: [{color: '#ebe3cd'}]
        },
        {
            featureType: 'transit.station',
            elementType: 'geometry',
            stylers: [{color: '#dfd2ae'}]
        },
        {
            featureType: 'water',
            elementType: 'geometry.fill',
            stylers: [{color: '#b9d3c2'}]
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{color: '#92998d'}]
        }
    ];
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 13,
        disableDefaultUI: true,
        styles: mapStyles
    });

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            currPos = pos;
            map.setCenter(pos);
            var icon = {
                url: '/resources/img/curr_loc.png',
                scaledSize: new google.maps.Size(36,36)
            };
            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                icon: icon
            });
        }, function() {
            handleLocationError();
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError();
    }
}

function handleLocationError() {
    console.log("Geolocation error");
}

function clearResultMarkers() {
    for(i=0; i<result_markers.length; i++){
        result_markers[i].setMap(null);
    }
    result_markers = [];
}

function renderResults(results) {
    var source   = $("#search-results-template").html();
    var template = Handlebars.compile(source);
    var html = template(results);
    $('#results').html(html);
    $('.front-wrapper').addClass('results-wrapper');
}

$( document ).ready(function() {
    $('#search-input').keypress(function (e) {
        if ( e.which === 13 ) {
            clearResultMarkers();
            const val = $(this).val();
            if (val.length < 3) {
                renderResults({'msg': 'Please enter more than 3 characters.'});
                return true;
            }
            const url = $(this).data('url') + val;
            $.ajax({
                url: url,
                data: currPos,
                success: function (data) {
                    const results = JSON.parse(data);
                    renderResults({'results': results});
                    const icon = {
                        url: '/resources/img/result_marker.svg',
                        scaledSize: new google.maps.Size(36,36)
                    };
                    results.forEach(function (result, i) {
                        result_markers[i] = new google.maps.Marker({
                            position: {
                                lat: result.lat,
                                lng: result.lng
                            },
                            map: map,
                            icon: icon
                        });
                    });
                    $('.search-result').hover(
                        function (e) {
                            const icon = {
                                url: '/resources/img/result_marker.svg',
                                scaledSize: new google.maps.Size(50,50)
                            };
                            const key = $(this).data('key');
                            result_markers[key].setIcon(icon);
                            map.setCenter(result_markers[key].getPosition());
                        },
                        function (e) {
                            const icon = {
                                url: '/resources/img/result_marker.svg',
                                scaledSize: new google.maps.Size(36,36)
                            };
                            const key = $(this).data('key');
                            result_markers[key].setIcon(icon);
                        }
                    ).click(function (e) {
                        const id = $(this).data('pk');
                        window.location = "/SingleListing/"+id;
                    });
                }
            });
        }
    });
});