package app.dao;

import app.model.Listing;
import app.model.NearbyListing;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ListingDAOImpl implements ListingDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public ListingDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void addListing(Listing listing) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(listing);
    }

    @SuppressWarnings("unchecked")
    public List<Listing> listListings() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "FROM Listing";
        List<Listing> ListingsList = session.createQuery(hql).list();
        return ListingsList;
    }

    @SuppressWarnings("unchecked")
    public List<Listing> getNearbyListings(
            String search,
            int radius,
            double latitude,
            double longitude
    ) {
        Session session = sessionFactory.getCurrentSession();
        List<NearbyListing> listings = new ArrayList<NearbyListing>();
        String hql = "SELECT new app.model.NearbyListing(l.id, -33.9029964, " +
                "151.184394," +
                " l.lat, l.lng)"
                + " FROM Listing l"
                + " WHERE l.name LIKE :search"
                + " OR l.keywords like :search";
        Query query = session.createQuery(hql);
        query.setParameter("search", "%" + search + "%");
        listings = query.list();
        List<Listing> returnListings = new ArrayList<Listing>();
        for (NearbyListing listing : listings) {
            System.out.println(listing.getDistance());
            if (listing.getDistance() < radius) {
                returnListings.add(this.getListingById(listing.getId()));
            }
        }
        return returnListings;
    }

    public void removeListing(int id) {
        Listing listing = getListingById(id);
        if (listing != null) {
            sessionFactory.getCurrentSession().delete(listing);
        }
    }

    public void updateListing(Listing listing) {
        Session session = sessionFactory.getCurrentSession();
        session.update(listing);
    }

    public Listing getListingById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Listing.class, id);
    }
}
