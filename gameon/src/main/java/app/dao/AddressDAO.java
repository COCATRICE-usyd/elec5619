package app.dao;

import app.model.Address;

import java.util.List;

public interface AddressDAO {
    void addAddress(Address address);

    void updateAddress(Address address);

    Address getAddressById(int id);

    void removeAddress(int id);

    List<Address> listAddresses();
}
