package app.dao;

import app.model.Address;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AddressDAOImpl implements AddressDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public AddressDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void addAddress(Address address) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(address);
    }

    public void updateAddress(Address address) {
        Session session = sessionFactory.getCurrentSession();
        session.update(address);
    }

    public Address getAddressById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Address address = (Address) session.get(Address.class, new Integer(id));
        return address;
    }

    public void removeAddress(int id) {
        Session session = sessionFactory.getCurrentSession();
        Address address = (Address) session.get(Address.class, new Integer(id));
        if(null != address){
            session.delete(address);
        }
    }

    @SuppressWarnings("unchecked")
    public List<Address> listAddresses() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "FROM Address";
        return session.createQuery(hql).list();
    }
}
