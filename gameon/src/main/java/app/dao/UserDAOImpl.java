package app.dao;

import app.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO{

    private final SessionFactory sessionFactory;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserDAOImpl(SessionFactory sessionFactory, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.sessionFactory = sessionFactory;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void addUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        session.persist(user);
    }

    public void updateUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }

    @SuppressWarnings("unchecked")
    public List<User> listUsers() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "FROM User";
        List<User> UsersList = session.createQuery(hql).list();
        return UsersList;
    }

    public User getUserById(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, new Integer(id));
        return user;
    }

    @SuppressWarnings("unchecked")
    public User getUserByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        List<User> userList = new ArrayList<User>();
        String hql = "from User u where u.username = :username";
        Query query = session.createQuery(hql);
        query.setParameter("username", username);
        userList = query.list();
        if (userList.size() > 0) {
            return userList.get(0);
        } else {
            return null;
        }

    }

    public void removeUser(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, new Integer(id));
        if(null != user){
            session.delete(user);
        }
    }
}
