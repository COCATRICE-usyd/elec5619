package app.dao;

import app.model.Message;
import app.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MessageDAOImpl implements MessageDAO{

    @Autowired
    private SessionFactory sessionFactory;

    public void sendMessage(Message message) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(message);
    }

    public void updateMessage(Message message) {
        Session session = sessionFactory.getCurrentSession();
        session.update(message);
    }


    public void deleteMessage(Message message) {
        Session session = sessionFactory.getCurrentSession();
        session.update(message);
    }

    @SuppressWarnings("unchecked")
    public List<Message> listMessages() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "FROM Message";
        List<Message> MessageList = session.createQuery(hql).list();
        return MessageList;
    }

    public Message getMessagebyID(int id) {
        Session session = sessionFactory.getCurrentSession();
        Message message = (Message) session.get(Message.class, new Integer(id));
        return message;
    }

    public List<Message> getMessagebyreceiver(String receiver) {

        Session session = sessionFactory.getCurrentSession();
        List<Message> userList = new ArrayList<Message>();
        String hql = "from Message m where m.receiver = :receiver";
        Query query = session.createQuery(hql);
        query.setParameter("receiver", receiver);
        userList = query.list();
        return userList;
    }

    public List<Message> getMessagebysender(String sender) {

        Session session = sessionFactory.getCurrentSession();
        List<Message> userList = new ArrayList<Message>();
        String hql = "from Message m where m.sender = :sender";
        Query query = session.createQuery(hql);
        query.setParameter("sender", sender);
        userList = query.list();
        return userList;
    }
}
