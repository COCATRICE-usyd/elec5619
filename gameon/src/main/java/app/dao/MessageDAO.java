package app.dao;

import app.model.Message;
import app.model.User;

import java.util.List;


public interface MessageDAO {

    void sendMessage(Message message);

    void updateMessage(Message message);

    void deleteMessage(Message message);

    List<Message> getMessagebyreceiver(String receiver);

    List<Message> getMessagebysender(String sender);

    Message getMessagebyID(int id);

    List<Message> listMessages();
}
