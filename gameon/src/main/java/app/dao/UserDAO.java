package app.dao;

import app.model.Login;
import app.model.User;

import java.util.List;

public interface UserDAO {
    void addUser(User employee);

    void updateUser(User employee);

    User getUserById(int id);

    User getUserByUsername(String username);

    void removeUser(int id);

    List<User> listUsers();
//
//    void register (User user);
//
//    User validateUser(Login login);

}
