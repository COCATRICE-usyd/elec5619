package app.dao;

import app.model.Listing;
import java.util.List;

public interface ListingDAO {
    void addListing(Listing listing);
    void removeListing(int id);
    void updateListing(Listing listing);
    Listing getListingById(int id);
    List<Listing> listListings();
    List<Listing> getNearbyListings(String search, int radius, double latitude,
                                    double longitude);
}
