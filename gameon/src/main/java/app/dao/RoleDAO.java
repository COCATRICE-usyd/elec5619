package app.dao;

import app.model.Role;

public interface RoleDAO {
    Role getRole(int id);
}
