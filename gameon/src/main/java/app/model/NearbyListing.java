package app.model;

public class NearbyListing {
    private int id;
    private double lat;
    private double lng;
    private double addrLat;
    private double addrLng;
    private double distance;

    public NearbyListing(int id) {
        this.id = id;
    }

    public NearbyListing(
            int id,
            double lat,
            double lng,
            double addrLat,
            double addrLng
    ) {
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.addrLat = addrLat;
        this.addrLng = addrLng;
        this.distance = calculateDistance(lat, lng, addrLat, addrLng);
    }

    private double calculateDistance(
            double lat,
            double lng,
            double addrLat,
            double addrLng
    ) {
        // cos( radians(lat) ) * cos( radians( addrlat ) )
        double sec1 = Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(addrLat));

        // cos( radians( addrLng ) - radians(lng) )
        double sec2 = Math.cos(Math.toRadians(addrLng) - Math.toRadians(lng));

        //sin( radians(lat) )
        double sec3 = Math.sin(Math.toRadians(lat));

        //sin( radians( addrLat ) )
        double sec4 = Math.sin(Math.toRadians(addrLat));

        return 6371 * Math.acos(sec1 * sec2 + sec3 * sec4);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
