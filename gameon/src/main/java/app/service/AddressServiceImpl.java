package app.service;

import app.dao.AddressDAO;
import app.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressDAO addressDAO;

    @Autowired
    public AddressServiceImpl(AddressDAO addressDAO) {
        this.addressDAO = addressDAO;
    }

    @Transactional
    public void addAddress(Address address) {
        addressDAO.addAddress(address);
    }

    @Transactional
    public void updateAddress(Address address) {
        addressDAO.updateAddress(address);
    }

    @Transactional
    public Address getAddressById(int id) {
        return addressDAO.getAddressById(id);
    }

    @Transactional
    public void removeAddress(int id) {
        addressDAO.removeAddress(id);
    }

    @Transactional
    public List<Address> listAddresses() {
        return addressDAO.listAddresses();
    }
}
