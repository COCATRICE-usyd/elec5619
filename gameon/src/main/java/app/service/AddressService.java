package app.service;

import app.model.Address;

import java.util.List;

public interface AddressService {
    void addAddress(Address address);

    void updateAddress(Address address);

    Address getAddressById(int id);

    void removeAddress(int id);

    List<Address> listAddresses();
}
