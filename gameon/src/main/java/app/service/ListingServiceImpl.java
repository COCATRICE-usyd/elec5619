package app.service;

import app.dao.ListingDAO;
import app.model.Listing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ListingServiceImpl implements ListingService {

    private final ListingDAO listingDAO;

    @Autowired
    public ListingServiceImpl(ListingDAO listingDAO){
        this.listingDAO = listingDAO;
    }

    @Transactional
    public void addListing(Listing listing) {
        listingDAO.addListing(listing);
    }

    @Transactional
    public void removeListing(int id) {
        listingDAO.removeListing(id);
    }
    @Transactional
    public void updateListing(Listing listing) {
        listingDAO.updateListing(listing);
    }

    @Transactional
    public Listing getListingById(int id) {
        return  listingDAO.getListingById(id);
    }

    @Transactional
    public List<Listing> listListings() {
        return this.listingDAO.listListings();
    }

    @Transactional
    public List<Listing> getNearbyListings(
            String search,
            int radius,
            double latitude,
            double longitude) {
        return listingDAO.getNearbyListings(search, radius, latitude,
                longitude);
    }

}
