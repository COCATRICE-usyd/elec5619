package app.service;

import app.model.Message;
import app.model.User;

import java.util.List;

public interface MessageService {

    void sendMessage(Message message);

    void updateMessage(Message message);

    void deleteMessage(Message message);

    Message getMessagebyID(int id);

    List<Message> getMessagebyreceiver(String receiver);

    List<Message> getMessagebysender(String sender);

    List<Message> listMessages();


}