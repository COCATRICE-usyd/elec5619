package app.service;


import app.dao.MessageDAO;
import app.dao.UserDAO;
import app.model.Message;
import app.model.User;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDAO messageDAO;

    public void setMessageDAO(MessageDAO messageDAO) {
        this.messageDAO = messageDAO;
    }

    @Transactional
    public void sendMessage(Message message) {
        messageDAO.sendMessage(message);
    }

    @Transactional
    public void updateMessage(Message message) { messageDAO.updateMessage(message); }

    @Transactional
    public void deleteMessage(Message message) {
        messageDAO.deleteMessage(message);
    }

    @Transactional
    public List<Message> listMessages() {
        return this.messageDAO.listMessages();
    }

    @Transactional
    public Message getMessagebyID(int id) {
        return messageDAO.getMessagebyID(id);
    }

    @Transactional
    public List<Message> getMessagebyreceiver(String receiver) { return  messageDAO.getMessagebyreceiver(receiver); }

    @Transactional
    public List<Message> getMessagebysender(String sender) { return messageDAO.getMessagebysender(sender); }

}

