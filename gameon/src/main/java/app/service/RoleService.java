package app.service;

import app.model.Role;

public interface RoleService {
    Role getRole(int id);
}
