package app.service;


import app.model.User;
import java.util.List;

public interface UserService {

    void addUser(User user);

    void updateUser(User user);

    User getUserById(int id);

    void removeUser(int id);

    User getUserByUsername(String username);

    List<User> listUsers();
}
