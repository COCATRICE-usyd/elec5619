package app.controllers;

import app.model.Address;
import app.model.Listing;
import app.service.AddressService;
import app.service.ListingService;
import app.model.User;
import app.service.UserService;
import jdk.nashorn.internal.parser.JSONParser;
import org.hibernate.Hibernate;
import org.json.JSONArray;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.MalformedURLException;
import java.security.Principal;
import java.util.List;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;

import static org.hibernate.Hibernate.initialize;

@Controller
public class UserController {


    @Autowired
    public UserService userService;

    @Autowired
    public ListingService listingService;

    @Autowired
    public AddressService addressService;

    @Transactional
    @RequestMapping(value = "/UserDashboard", method = RequestMethod.GET)
    public ModelAndView showUserDash(Authentication authentication) {
        ModelAndView mav = new ModelAndView("UserDashboard");
        String username = authentication.getName();
        User user = userService.getUserByUsername(username);
        Address address = user.getAddress();
        System.out.println(address.toString());
        List<Listing> listings = user.getListings();
        Boolean is_list = true;
        if(listings.isEmpty())
        {
            is_list = false;
        }

        mav.addObject("listings", listings);
        mav.addObject("user", user);
        mav.addObject("address", address);
        mav.addObject("listing", new Listing());
        mav.addObject("is_list", is_list);
//        mav.addObject("user", new User());

        return mav;
    }

    @RequestMapping(value = "/AddListing", method = RequestMethod.POST)
    public ModelAndView addListing(@ModelAttribute("listing") Listing listing, Authentication authentication) {
        String username = authentication.getName();
        User user = userService.getUserByUsername(username);
        listing.setUser(user);
        Double lat = user.getAddress().getLat();
        Double lng = user.getAddress().getLng();
        listing.setLat(lat);
        listing.setLng(lng);
        listingService.addListing(listing);
        return new ModelAndView("redirect:/UserDashboard");
    }

    @Transactional
    @RequestMapping(value = "/UpdateUser", method = RequestMethod.POST)
    public ModelAndView updateUser(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,  @RequestParam("street") String street,  @RequestParam("city") String city,  @RequestParam("state") String state,  @RequestParam("country") String country,  Authentication authentication) {
        System.out.println("-------- /UpdateUser called ---------");
        String username = authentication.getName();
        User user = userService.getUserByUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.getAddress().setStreet(street);
        user.getAddress().setCity(city);
        user.getAddress().setState(state);
        user.getAddress().setCountry(country);

        userService.updateUser(user);

        return new ModelAndView("redirect:/UserDashboard");
    }



}

