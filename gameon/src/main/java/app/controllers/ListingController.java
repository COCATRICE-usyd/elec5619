package app.controllers;

import app.model.Listing;
import app.model.User;
import app.service.ListingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class ListingController {

    @Autowired
    public ListingService listingService;

    @RequestMapping(value = "/AddListing", method = RequestMethod.GET)
    public ModelAndView showWelcome(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("AddListing");
        mav.addObject("listing", new Listing());
        return mav;
    }

    @Transactional
    @RequestMapping(value = "/SingleListing/{listing_id}",  method = RequestMethod.GET)
    public ModelAndView showUserDash(Authentication authentication, @PathVariable int listing_id) {
        ModelAndView mav = new ModelAndView("SingleListing");
        System.out.print("------ listing id: ");
        System.out.println(listing_id);
        Listing listing = listingService.getListingById(listing_id);
        System.out.print("------ listing name: ");
        System.out.println(listing.getName());
        mav.addObject("listing", listing);
        return mav;
    }



//    @RequestMapping(value = "/AddListing", method = RequestMethod.POST)
//    public ModelAndView addListing(@ModelAttribute("listing") Listing listing) {
//        listingService.addListing(listing);
//        return new ModelAndView("redirect:/");
//    }
}
