package app.controllers;

import app.model.User;
import app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WelcomeController {

    private final UserService userService;

    @Autowired
    public WelcomeController(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showWelcome() {
        ModelAndView mav = new ModelAndView("LandingPage");
        mav.addObject("user", new User());
        return mav;
    }


//    @RequestMapping(value = "/UserDashboard", method = RequestMethod.GET)
//    public ModelAndView showUserDash(HttpServletRequest request, HttpServletResponse response) {
//        ModelAndView Dash = new ModelAndView("UserDashboard");
////        mav.addObject("user", new User());
//        return Dash;
//    }

}



