package app.controllers;

import app.model.Address;
import app.model.Listing;
import app.model.User;
import app.service.ListingService;
import app.service.UserService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    private final UserService userService;
    private final ListingService listingService;

    @Autowired
    public HomeController(
            UserService userService,
            ListingService listingService
    ) {
        this.userService = userService;
        this.listingService = listingService;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String showSearch(Model model) {
        return "search";
    }

    @RequestMapping(value = "/api/search/{search}", method = RequestMethod.GET)
    @ResponseBody
    public String searchData(
            @PathVariable("search") String search,
            @RequestParam("lat") double lat,
            @RequestParam("lng") double lng
    ) {
        List<Listing> listings = listingService.getNearbyListings(
                search,
                5,
                lat,
                lng
        );
        JSONArray listingsJson = new JSONArray();
        for (Listing listing: listings) {
            JSONObject listingJson = new JSONObject();
            listingJson.put("lat", listing.getLat());
            listingJson.put("lng", listing.getLng());
            listingJson.put("name", listing.getName());
            listingJson.put("description", listing.getDescription());
            listingJson.put("id", listing.getId());
            listingJson.put("price", listing.getPrice());
            listingsJson.put(listingJson);
        }
        return listingsJson.toString();
    }

    @RequestMapping(value = "/create-data", method = RequestMethod.GET)
    @ResponseBody
    public String createData(Model model) {
        User user = new User();
        user.setFirstName("Bruce");
        user.setLastName("Wayne");

        Address address = new Address();
        address.setStreet("138 Carillon Rd");
        address.setCity("Newtown");
        address.setState("NSW");
        address.setCountry("Australia");
        user.setAddress(address);

        List<Listing> listings = new ArrayList<Listing>();
        for (int i = 1; i < 5; i++) {
            Listing listing = new Listing();
            listing.setName("Uncharted " + i);
            listing.setDescription("The best game in the franchise");
            listing.setKeywords("ps4,games,uncharted,adventure,action," +
                    "playstation");
            listing.setPrice(2.50f);
            listings.add(listing);
        }
        user.setListings(listings);
        userService.addUser(user);
        return "created";
    }
}
