package app.controllers;

import app.model.Message;
import app.model.User;
import app.service.MessageService;
import app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import sun.plugin2.os.windows.SECURITY_ATTRIBUTES;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MessengerController {

    @Autowired
    public MessageService messageService;

    @Autowired
    public UserService userService;


    //
//    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
//    public String Welcome(Model model) {
//        return "LandingPage";
//    }
    @RequestMapping(value = {"/messenger", "/messenger/inbox"}, method = RequestMethod.GET)
    public ModelAndView welcome(Authentication authentication) {
        ModelAndView mav = new ModelAndView("Messenger");
        String view = new String("inbox");
        String username = authentication.getName();
        User user = userService.getUserByUsername(username);
        List<Message> messageList = new ArrayList<Message>();
        messageList = messageService.getMessagebyreceiver(user.getUsername());
        mav.addObject("messageList", messageList);
//        User user = session.getAttribute("user");
//        mav.addObject("user", user);

//        mav.addObject("username", username);
        mav.addObject("user", user);
        mav.addObject("view", view);
//        mav.addObject("messagei", new Message());
        return mav;
    }

    @RequestMapping(value = {"/messenger", "/messenger/inbox"}, method = RequestMethod.POST, params = {"msg_delete_in_id"})
    public ModelAndView inboxdelete(Authentication authentication, @RequestParam("msg_delete_in_id") int id ) {

        Message message_delete = messageService.getMessagebyID(id);
        message_delete.setIsdeleted(2);
//        msg.setIsdeleted(2);
        messageService.updateMessage(message_delete);
        return new ModelAndView("redirect:/messenger/trash");


    }


    @RequestMapping(value = {"/messenger", "/messenger/inbox"}, method = RequestMethod.POST, params = {"msg_star_in_id"})
    public ModelAndView inboxstar(Authentication authentication, @RequestParam("msg_star_in_id") int id ) {

        Message message_star = messageService.getMessagebyID(id);
        message_star.setStar(true);
//        msg.setIsdeleted(2);
        messageService.updateMessage(message_star);
        return new ModelAndView("redirect:/messenger/star");

    }


    @RequestMapping(value = "/messenger/outbox", method = RequestMethod.GET)
    public ModelAndView outbox(Authentication authentication) {
        ModelAndView mav = new ModelAndView("Messenger");
        String view = new String("outbox");
        String username = authentication.getName();
        User user = userService.getUserByUsername(username);
        List<Message> messageList = new ArrayList<Message>();
        messageList = messageService.getMessagebysender(user.getUsername());
        mav.addObject("messageListO", messageList);
        mav.addObject("user", user);
        mav.addObject("view", view);
        return mav;
    }

    @RequestMapping(value = "/messenger/outbox", method = RequestMethod.POST, params = {"msg_delete_out_id"})
    public ModelAndView outboxstar(Authentication authentication, @RequestParam("msg_delete_out_id") int id ) {

        Message message_delete = messageService.getMessagebyID(id);
        message_delete.setIsdeleted(2);
//        msg.setIsdeleted(2);
        messageService.updateMessage(message_delete);
        return new ModelAndView("redirect:/messenger/trash");


    }

    @RequestMapping(value = "/messenger/outbox", method = RequestMethod.POST, params = {"msg_star_out_id"})
    public ModelAndView outboxdelete(Authentication authentication, @RequestParam("msg_star_out_id") int id ) {

        Message message_star = messageService.getMessagebyID(id);
        message_star.setStar(true);
//        msg.setIsdeleted(2);
        messageService.updateMessage(message_star);
        return new ModelAndView("redirect:/messenger/star");

    }

//    @RequestMapping(value = "/messenger/inbox", method = RequestMethod.POST)
//    public ModelAndView outboxdelete(Authentication authentication) {
//
//    }

    @RequestMapping(value = "/messenger/new", method = RequestMethod.GET)
    public ModelAndView send(Authentication authentication) {
        ModelAndView mav = new ModelAndView("Messenger");
        String view = new String("new");
        String username = authentication.getName();
        User user = userService.getUserByUsername(username);
        mav.addObject("user", user);
        mav.addObject("message", new Message());
        mav.addObject("view", view);
        return mav;
    }

    @RequestMapping(value = "/messenger/new", method = RequestMethod.POST)
    public ModelAndView sendmessage(Authentication authentication, @ModelAttribute Message message) {
        String sender = authentication.getName();
        message.setSender(sender);
        messageService.sendMessage(message);
        return new ModelAndView("redirect:/messenger/inbox");
    }

    @RequestMapping(value = "/messenger/trash", method = RequestMethod.GET)
    public ModelAndView trash(Authentication authentication) {
        ModelAndView mav = new ModelAndView("Messenger");
        String view = new String("trash");
        String username = authentication.getName();
        User user = userService.getUserByUsername(username);
        List<Message> messageList = new ArrayList<Message>();
        messageList = messageService.getMessagebyreceiver(user.getUsername());
        List<Message> messageListo = new ArrayList<Message>();
        messageListo = messageService.getMessagebysender(user.getUsername());
        mav.addObject("messageListt", messageList);
        mav.addObject("messageListto", messageListo);
        mav.addObject("user", user);
        mav.addObject("view", view);
        return mav;
    }

    @RequestMapping(value = "/messenger/trash", method = RequestMethod.POST)
    public ModelAndView trashrecover(Authentication authentication, @RequestParam("msg_recover_id") int id ) {

        Message message_delete = messageService.getMessagebyID(id);
        message_delete.setIsdeleted(1);
//        msg.setIsdeleted(2);
        messageService.updateMessage(message_delete);
        return new ModelAndView("redirect:/messenger/inbox");


    }


    @RequestMapping(value = "/messenger/star", method = RequestMethod.GET)
    public ModelAndView star(Authentication authentication) {

        ModelAndView mav = new ModelAndView("Messenger");
        String view = new String("star");
        String username = authentication.getName();
        User user = userService.getUserByUsername(username);
        List<Message> messageListsi = new ArrayList<Message>();
        messageListsi = messageService.getMessagebyreceiver(user.getUsername());
        List<Message> messageListso = new ArrayList<Message>();
        messageListso = messageService.getMessagebysender(user.getUsername());
        mav.addObject("messageListsi", messageListsi);
        mav.addObject("messageListso", messageListso);
        mav.addObject("user", user);
        mav.addObject("view", view);
        return mav;
    }

    @RequestMapping(value = "/messenger/star", method = RequestMethod.POST, params = {"msg_unstar_id"})
    public ModelAndView unstar(Authentication authentication, @RequestParam("msg_unstar_id") int id ) {

        Message message_star = messageService.getMessagebyID(id);
        message_star.setStar(false);
//        msg.setIsdeleted(2);
        messageService.updateMessage(message_star);
        return new ModelAndView("redirect:/messenger/inbox");

    }



}
