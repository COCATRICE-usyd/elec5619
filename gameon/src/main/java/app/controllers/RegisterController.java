package app.controllers;

import app.model.Address;
import app.model.User;
import app.service.RoleService;
import app.service.UserService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

@Controller
public class RegisterController {

    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public RegisterController(
            UserService userService,
            RoleService roleService
    ) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView showWelcome() {
        ModelAndView mav = new ModelAndView("register");
        User user = new User();
        mav.addObject("user", user);
        return mav;
    }

    @Transactional
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView addUser(@ModelAttribute("user") User user, @RequestParam("street") String street,  @RequestParam("city") String city,  @RequestParam("state") String state,  @RequestParam("country") String country) {
        user.setRole(roleService.getRole(2));
        Address address = new Address();
        user.setAddress(address);
        user.getAddress().setStreet(street);
        user.getAddress().setCity(city);
        user.getAddress().setState(state);
        user.getAddress().setCountry(country);

        String url = "https://maps.googleapis.com/maps/api/geocode/json?address="+street.replaceAll(" ","+")+",+"+city+",+"+state+"&key=AIzaSyBsA9XtShtKkVC11syeDEwgh0IAl6vln5o";
        System.out.print("----- URL: ");
        System.out.println(url);
        try {
            URL domain = new URL(url);
            InputStream is = domain.openStream();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                String jsonText = readAll(rd);
                JSONObject json = new JSONObject(jsonText);
                JSONObject geom = (JSONObject) ((JSONObject)((JSONArray) json.get("results")).get(0)).get("geometry");
                JSONObject location = (JSONObject) geom.get("location");
                double lat = ((Number)location.get("lat")).doubleValue();
                double lng = ((Number)location.get("lng")).doubleValue();
                System.out.print("Google returned object:");
                System.out.println(json.toString());
                System.out.print("lat: ");
                System.out.println(lat);
                System.out.print("lng: ");
                System.out.println(lng);
                user.getAddress().setLat(lat);
                user.getAddress().setLng(lng);

            } finally {
                is.close();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        userService.addUser(user);
        return new ModelAndView("redirect:/perform_login");
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
    }
