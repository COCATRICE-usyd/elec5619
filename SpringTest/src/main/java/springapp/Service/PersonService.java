package springapp.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springapp.DAO.PersonDao;
import springapp.domain.Person;

/**
 * Created by Cocatrice on 22/8/17.
 */

@Service(value="personService")
// @Transactional
public class PersonService {

    @Autowired
    private PersonDao personDao;

    // business logic of registering a Person into the database
    public void registerPerson(Person person) {

        // Step 1: check whether this person is already in the database

        // Step 2: if not, save this person into the database
        personDao.savePerson(person);
    }

}
