package springapp.DAO;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import springapp.domain.Person;

import javax.annotation.Resource;

/**
 * Created by Cocatrice on 22/8/17.
 */
@Repository(value = "personDao")
public class PersonDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void savePerson(Person person) {
        sessionFactory.getCurrentSession().save(person);
    }
}
