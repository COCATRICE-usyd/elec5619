package springapp.Web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Created by Cocatrice on 9/8/17.
 */
@Controller
public class IndexController {
    @RequestMapping("/test")
    public String showIndex(){
        return "Index";
    }

}
