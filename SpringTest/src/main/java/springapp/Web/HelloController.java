package springapp.Web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * Created by Cocatrice on 15/8/17.
 */
@Controller
public class HelloController {
    @RequestMapping("/hello")
//    public String ShowTime(){
//        String now = (new Date()).toString();
//        return "hello";
//    }
    public ModelAndView ShowTime(){
        String now = (new Date()).toString();
        return new ModelAndView("hello", "now", now);
    }
}
